
1.Today, we mainly conducted a code review of last Friday's homework, shared three design patterns, and finally explained some concepts and exercises related to refactoring:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1) The first is code review. Today, through code presentation, I shared the idea of using Strategy pattern in my homework with my classmates, consolidating my knowledge.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2) Then there is the sharing of three design modes, which mainly explains the Strategy pattern, Observer pattern, and Command pattern. Because the ideas of these design modes are useful in some previous assignments, it will not be difficult to learn today.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(3) Finally, there is Refactoring, which I have learned about its concept, purpose, and process, common code smarts, and how to refactor.  
2.In today's learning, I found that my understanding of design patterns still needs improvement.  
3.I have found that my understanding of design patterns is still lacking. Through today's learning, I have gained a deeper understanding of design patterns.  
4.In practical development, there are many places where design patterns are used, and I hope to use them reasonably in software development in the future.  