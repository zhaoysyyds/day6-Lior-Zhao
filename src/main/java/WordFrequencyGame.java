import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public String getResult(String inputStr) {
        try {
            //split the input string with 1 to n pieces of spaces
            List<Input> inputList = transformToInputs(inputStr);

            //get the map for the next step of sizing the same word
            inputList = getSameWordInputList(inputList);

            inputList.sort((input1, input2) -> input2.getWordCount() - input1.getWordCount());

            return getStringResult(inputList);
        } catch (Exception e) {
            return "Calculate Error";
        }

    }
    private String getStringResult(List<Input> inputList) {
        return inputList.stream().map(input -> input.getValue() + " " + input.getWordCount()).collect(Collectors.joining("\n"));
    }

    private List<Input> transformToInputs(String inputStr) {
        String[] words = inputStr.split("\\s+");
        return Arrays.stream(words).map(word -> new Input(word, 1)).collect(Collectors.toList());
    }

    private List<Input> getSameWordInputList(List<Input> inputList) {
        Map<String, Integer> sameWordMap = new HashMap<>();
        inputList.stream().forEach(input -> sameWordMap.put(input.getValue(), sameWordMap.getOrDefault(input.getValue(), 0) + 1));
        List<Input> sameWordList = new ArrayList<>();
        sameWordMap.forEach((key, value) -> sameWordList.add(new Input(key, value)));
        return sameWordList;
    }


}
